import React from "react";
import {Button} from 'react-bootstrap';
const _Button = props => {
    const {onClick, title, icon} = props
    
    return (
        <Button
            variant={"link"}
            className={"mx-1 p-0"}
            onClick={onClick}
            style={{ height: 40, width: 40 }}
            title={title}
        >
            <img
                src={icon}
                alt={"icon"}
                className={"w-100 h-100"}
            />
        </Button>
    );
};

export default _Button;