import React from "react";

import Button from "../Button/Button";
import { todoActions } from "../../boot/actions/todoActions";
import { useAppState } from "../../boot/store/store";

const ActionTodos = props => {
    const { type, todo } = props;
    const [appState, dispatch] = useAppState()
    const {
        todoTrash,
        todoComplete,
        todoEdit,
        todoSubmitEditing,
        todoCancelEditing,
        todoNotComplete,
        todoDelete,
        todoRestore
    } = todoActions;

    const handleTodoTrash = () => {
        dispatch(todoTrash(todo));
    };

    const handleTodoDone = () => {
        dispatch(todoComplete(todo));
    };

    const handleTodoEdit = () => {
        dispatch(todoEdit(todo));
    };

    const handleTodoCancelEditing = () => {
        dispatch(todoCancelEditing(todo));
    };

    const handleTodoSubmitEditing = () => {
        dispatch(todoSubmitEditing(todo));
    };

    const handleTodoNotDone = () => {
        dispatch(todoNotComplete(todo));
    };

    const handleTodoRestore = ()=> {
        dispatch(todoRestore(todo))
    }

    const handleTodoDelete = ()=> {
        dispatch(todoDelete(todo))
    }

    if (type === "editActions") {
        return (
            <div>
                <Button
                    onClick={handleTodoSubmitEditing}
                    icon={"/assets/icons/checked.svg"}
                    title={"تایید"}
                />
                <Button
                    onClick={handleTodoCancelEditing}
                    icon={"/assets/icons/cancel.svg"}
                    title={"انصراف"}
                />
            </div>
        );
    }

    if (type === "doneActions") {
        return (
            <div>
                <Button
                    onClick={handleTodoNotDone}
                    icon={"/assets/icons/restore.svg"}
                    title={"هنوز تمام نشده"}
                />
            </div>
        );
    }

    if (type === "trashActions") {
        return (
            <div>
                <Button
                    onClick={handleTodoRestore}
                    icon={"/assets/icons/restore.svg"}
                    title={"بازیابی"}
                />
                <Button
                    onClick={handleTodoDelete}
                    icon={"/assets/icons/cancel.svg"}
                    title={"برای همیشه پاک شود"}
                />
            </div>
        );
    }

    return (
        <div>
            <Button
                onClick={handleTodoTrash}
                icon={"/assets/icons/garbage.svg"}
                title={"حذف"}
            />
            <Button
                onClick={handleTodoDone}
                icon={"/assets/icons/checked.svg"}
                title={"انجام شد"}
            />
            <Button
                onClick={handleTodoEdit}
                icon={"/assets/icons/edit.svg"}
                title={"ویرایش"}
            />
        </div>
    );
};

export default ActionTodos;
