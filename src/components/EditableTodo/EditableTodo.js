import React from "react";
import { FormControl } from "react-bootstrap";

import ActionTodos from "../ActionTodos/ActionTodos";
import { todoActions } from "../../boot/actions/todoActions";
import { useAppState } from "../../boot/store/store";

const EditableTodo = props => {
    const [appState, dispatch] = useAppState()
    const { todo, actionType } = props;
    const {todoEditing} = todoActions;


    const handleTodoEdit = e => {
        dispatch(todoEditing(todo, e.target.value));
    };

    if (todo.isEditing) {
        return (
            <>
                <FormControl
                    className={'flex-grow-1 w-auto'}
                    value={todo.editingTodo}
                    onChange={handleTodoEdit}
                />
                <ActionTodos type={"editActions"} todo={todo} />
            </>
        );
    }

    return (
        <>
            <div>{todo.todo}</div>
            <ActionTodos type={actionType} todo={todo} />
        </>
    );
};

export default EditableTodo;
