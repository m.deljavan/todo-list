import {
    TODO_ADD,
    TODO_TRASH,
    TODO_DELETE,
    TODO_EDIT,
    TODO_RESTORE,
    TODO_NOT_DONE,
    TODO_DEFINITION,
    TODO_EDITING,
    TODO_DONE,
    TODO_CANCEL_EDITING,
    TODO_SUBMIT_EDITING
} from "./actionType";

export const todoActions = {
    todoDefinition: todo => {
        return {
            type: TODO_DEFINITION,
            todo
        };
    },
    todoAdd: () => {
        return {
            type: TODO_ADD
        };
    },
    todoTrash: todo => {
        return {
            type: TODO_TRASH,
            todo
        };
    },
    todoDelete: todo => {
        return {
            type: TODO_DELETE,
            todo
        };
    },
    todoEdit: todo => {
        return {
            type: TODO_EDIT,
            todo
        };
    },
    todoComplete: todo => {
        return {
            type: TODO_DONE,
            todo
        };
    },
    todoRestore: todo => {
        return {
            type: TODO_RESTORE,
            todo
        };
    },
    todoNotComplete: todo => {
        return {
            type: TODO_NOT_DONE,
            todo
        };
    },
    todoEditing: (todo, value) => {
        return {
            type: TODO_EDITING,
            todo,
            value
        };
    },
    todoSubmitEditing: todo => {
        return {
            type: TODO_SUBMIT_EDITING,
            todo
        };
    },
    todoCancelEditing: todo => {
        return {
            type: TODO_CANCEL_EDITING,
            todo
        };
    }
};
