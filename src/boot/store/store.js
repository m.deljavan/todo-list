import React,{useContext, useReducer,createContext} from 'react';
import { todoReducer, initialState } from '../reducers/todoReducer';

const appContext = createContext();

export const Provider = ({children}) => {
    const [appState, dispatch] = useReducer(todoReducer, initialState)
    return (
        <appContext.Provider value={[appState,dispatch]}>
            {children}
        </appContext.Provider>
    )
}

export const useAppState = () => useContext(appContext)