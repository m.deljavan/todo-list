import {
    TODO_TRASH,
    TODO_RESTORE,
    TODO_DELETE
} from "../actions/actionType";

const initialState = [];

export const trashedTodoReducer = (
    state = initialState,
    action = {}
) => {
    const { type } = action;

    switch (type) {
        case TODO_TRASH:
            return [
                ...state,
                {
                    ...action.todo
                }
            ];

        case TODO_DELETE:
            return state.filter(
                todo => todo.id !== action.todo.id
            );

        case TODO_RESTORE:
            return state.filter(
                todo => todo.id !== action.todo.id
            );
        default:
            return state;
    }
};
