import uuid from "uuid";

import {
    TODO_ADD,
    TODO_DEFINITION,
    TODO_DONE,
    TODO_DELETE,
    TODO_TRASH,
    TODO_NOT_DONE,
    TODO_RESTORE,
    TODO_EDIT,
    TODO_EDITING,
    TODO_CANCEL_EDITING,
    TODO_SUBMIT_EDITING
} from "../actions/actionType";

export const initialState = JSON.parse(localStorage.getItem('mda-todos')) || {
    activeTodos: [],
    trashedTodos: [],
    completedTodos: [],
    todoInput: ""
};

export const todoReducer = (state , action ) => {
    const { type } = action;
    
    switch (type) {
        case TODO_ADD:
            if (!state.todoInput){
                return state
            }
            return {
                ...state,
                todoInput: "",
                activeTodos: [
                    {
                        id: uuid.v4(),
                        todo: state.todoInput,
                        isEditing: false,
                        editingTodo: state.todoInput
                    },
                    ...state.activeTodos
                ]
            };
        case TODO_DEFINITION:
            return {
                ...state,
                todoInput: action.todo
            };
        case TODO_DONE:
            return {
                ...state,
                activeTodos: state.activeTodos.filter(
                    todo => todo.id !== action.todo.id
                ),
                completedTodos: [
                    ...state.completedTodos,
                    {
                        ...action.todo
                    }
                ]
            };
        case TODO_TRASH:
            return {
                ...state,
                activeTodos: state.activeTodos.filter(
                    todo => todo.id !== action.todo.id
                ),
                trashedTodos: [
                    ...state.trashedTodos,
                    {
                        ...action.todo
                    }
                ]
            };
        case TODO_DELETE:
            return {
                ...state,
                trashedTodos: state.trashedTodos.filter(
                    todo => todo.id !== action.todo.id
                )
            };
        case TODO_NOT_DONE:
            return {
                ...state,
                activeTodos: [
                    ...state.activeTodos,
                    {
                        ...action.todo
                    }
                ],
                completedTodos: state.completedTodos.filter(
                    todo => todo.id !== action.todo.id
                )
            };
        case TODO_RESTORE:
            return {
                ...state,
                activeTodos: [
                    ...state.activeTodos,
                    {
                        ...action.todo
                    }
                ],
                trashedTodos: state.trashedTodos.filter(
                    todo => todo.id !== action.todo.id
                )
            };
        case TODO_EDIT:
            return {
                ...state,
                activeTodos: state.activeTodos.map(todo =>
                    todo.id === action.todo.id
                        ? {
                              ...todo,
                              isEditing: true
                          }
                        : todo
                )
            };
        case TODO_EDITING:
            return {
                ...state,
                activeTodos: state.activeTodos.map(todo =>
                    todo.id === action.todo.id
                        ? {
                              ...todo,
                              editingTodo: action.value
                          }
                        : todo
                )
            };
        case TODO_SUBMIT_EDITING:
            return {
                ...state,
                activeTodos: state.activeTodos.map(todo =>
                    todo.id === action.todo.id
                        ? {
                              ...todo,
                              todo: todo.editingTodo ? todo.editingTodo: todo.todo,
                              isEditing: todo.editingTodo ? false : true
                          }
                        : todo
                )
            };
        case TODO_CANCEL_EDITING:
            return {
                ...state,
                activeTodos: state.activeTodos.map(todo =>
                    todo.id === action.todo.id
                        ? {
                              ...todo,
                              editingTodo: todo.todo,
                              isEditing: false
                          }
                        : todo
                )
            };
        default:
            return state;
    }
};
