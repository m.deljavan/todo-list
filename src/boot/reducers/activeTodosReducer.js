import uuid from "uuid";
import {
    TODO_ADD,
    TODO_DEFINITION,
    TODO_TRASH,
    TODO_EDIT,
    TODO_DONE,
    TODO_CANCEL_EDITING,
    TODO_EDITING,
    TODO_SUBMIT_EDITING,
    TODO_RESTORE,
    TODO_NOT_DONE
} from "../actions/actionType";

const initialState = {
    activeTodos: [
        {
            id: uuid.v4(),
            todo: "mda",
            isEditing: false,
            editingTodo: "mda"
        }
    ],
    todoInput: ""
};
export const activeReducerTodos = (
    state = initialState,
    action = {}
) => {
    const { type } = action;

    switch (type) {
        case TODO_ADD:
            return {
                ...state,
                todoInput: "",
                activeTodos: [
                    {
                        id: uuid.v4(),
                        todo: state.todoInput,
                        isEditing: false,
                        editingTodo: ""
                    },
                    ...state.activeTodos
                ]
            };
        case TODO_DEFINITION:
            return {
                ...state,
                todoInput: action.todo
            };
        case TODO_DONE:
        case TODO_TRASH:
            return {
                ...state,
                activeTodos: state.activeTodos.filter(
                    todo => todo.id !== action.todo.id
                )
            };
        case TODO_EDIT:
        case TODO_CANCEL_EDITING:
            return {
                ...state,
                activeTodos: state.activeTodos.map(todo =>
                    todo.id === action.todo.id
                        ? {
                              ...todo,
                              isEditing: !todo.isEditing,
                              editingTodo: todo.todo
                          }
                        : todo
                )
            };
        case TODO_EDITING:
            return {
                ...state,
                activeTodos: state.activeTodos.map(todo =>
                    todo.id === action.todo.id
                        ? {
                              ...todo,
                              editingTodo: action.value
                          }
                        : todo
                )
            };
        case TODO_SUBMIT_EDITING:
            return {
                ...state,
                activeTodos: state.activeTodos.map(todo =>
                    todo.id === action.todo.id
                        ? {
                              ...todo,
                              todo: todo.editingTodo,
                              isEditing: false
                          }
                        : todo
                )
            };
        case TODO_RESTORE:
            return {
                ...state,
                activeTodos: [
                    ...state.activeTodos,
                    {
                        ...action.todo
                    }
                ]
            };
            case TODO_NOT_DONE:
            return {
                ...state,
                activeTodos: [
                    ...state.activeTodos,
                    {
                        ...action.todo
                    }
                ]
            };
        default:
            return state;
    }
};
