import { combineReducers } from "redux";
import { todoReducer } from "./todoReducer";
import { activeReducerTodos } from "./activeTodosReducer";
import { completedTodoReducer } from "./completedTodosReducer";
import { trashedTodoReducer } from "./trashedTodoreducers";

export const reducers = combineReducers({
    todoState: todoReducer,
    activeTodosState: activeReducerTodos,
    completedTodosState: completedTodoReducer,
    trashedTodosState: trashedTodoReducer 
}) 