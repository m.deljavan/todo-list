import {
    TODO_DONE,
    TODO_NOT_DONE
} from "../actions/actionType";

const initialState = [];

export const completedTodoReducer = (
    state = initialState,
    action = {}
) => {
    const { type } = action;

    switch (type) {
        case TODO_DONE:
            return [
                ...state,
                {
                    ...action.todo
                }
            ];

        case TODO_NOT_DONE:
            return state.filter(
                todo => todo.id !== action.todo.id
            );
        default:
            return state;
    }
};
