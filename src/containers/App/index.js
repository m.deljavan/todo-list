import React from "react";
import { Container } from "react-bootstrap";

import TodoList from "../TodoList/TodoList";
import { Provider } from "../../boot/store/store";

const App = props => {
  return (
    <Provider>
      <Container>
        <TodoList />
      </Container>
    </Provider>
  );
};

export default App;
