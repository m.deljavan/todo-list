import React, { useRef, useEffect, useState } from "react";
import EditableTodo from "../../components/EditableTodo/EditableTodo";

const Todos = props => {
    const {
        title,
        todos,
        actionType,
        showTodos,
        className
    } = props;
    const ulRef = useRef();
    const [height, setHeight] = useState("auto");
    const [active, setActive] = useState(showTodos);

    useEffect(() => {
        const currentActive = active;
        setTimeout(() => {
            setActive(true);
            setHeight("auto");
            setHeight(ulRef.current.clientHeight);
            setActive(currentActive);
        }, 0);
    }, [todos.length]);
    return (
        <div
            className={"text-right mt-2 pt-2 " + className}
        >
            <h3
                className={
                    "px-3 d-flex align-items-center justify-content-between"
                }
                style={{ cursor: "pointer" }}
                onClick={() => setActive(s => !s)}
            >
                <div>
                    <span>{title}</span>
                    <span
                        className={"mx-2"}
                        style={{ fontSize: "1rem" }}
                    >
                        ({todos.length})
                    </span>
                </div>
                <div>
                    <span
                        className={"plus minis"}
                        style={{
                            transform: active
                                ? "rotate(0)"
                                : "rotate(-90deg)"
                        }}
                    ></span>
                    <span
                        className={"minis position-relative"}
                    ></span>
                </div>
            </h3>
            <ul
                ref={ulRef}
                style={{
                    height: active ? height : 0,
                    overflow: "hidden",
                    transition: "all 200ms"
                }}
            >
                {todos.map(todo => (
                    <li
                        className={
                            "bg-dark text-white d-flex justify-content-between align-items-center px-3 py-2 my-1"
                        }
                        key={todo.id}
                    >
                        <EditableTodo
                            actionType={actionType}
                            todo={todo}
                        />
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default Todos;
