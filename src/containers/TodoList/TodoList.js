import React, { useEffect } from "react";
import { Form, FormControl } from "react-bootstrap";

import { todoActions } from "../../boot/actions/todoActions";
import Todos from "./Todos";
import { useAppState } from "../../boot/store/store";

const TodoList = props => {
    const [appState, dispatch] = useAppState();
    const { todoInput, activeTodos, trashedTodos, completedTodos } = appState;
    useEffect(() => {
        const newAppState = { ...appState };
        newAppState.activeTodos = newAppState.activeTodos.map(todo => ({
            ...todo,
            isEditing: false
        }));
        newAppState.todoInput = ''
        localStorage.setItem("mda-todos", JSON.stringify(newAppState));
    });
    const { todoDefinition, todoAdd } = todoActions;

    const handleChangeInput = e => {
        dispatch(todoDefinition(e.target.value));
    };

    const handleSubmit = e => {
        e.preventDefault();

        dispatch(todoAdd());
    };

    return (
        <div className={"mt-3"}>
            <Form onSubmit={handleSubmit}>
                <FormControl
                    value={todoInput}
                    onChange={handleChangeInput}
                    placeholder={"insert Todo"}
                />
            </Form>
            <div className={"pt-2 mt-2 text-white"}>
                {activeTodos.length > 0 && (
                    <Todos
                        todos={activeTodos}
                        actionType={"mainActions"}
                        title={"کارها"}
                        className={"bg-primary"}
                        showTodos={true}
                    />
                )}
                {completedTodos.length > 0 && (
                    <Todos
                        todos={completedTodos}
                        actionType={"doneActions"}
                        title={"کارهای انجام شده"}
                        className={"bg-warning"}
                    />
                )}
                {trashedTodos.length > 0 && (
                    <Todos
                        todos={trashedTodos}
                        actionType={"trashActions"}
                        title={"سطل آشغال"}
                        className={"bg-danger"}
                    />
                )}
            </div>
        </div>
    );
};

export default TodoList;
